package factory

import domain.AbsorptionTrait
import domain.Monster
import domain.Trait
import domain.types.DamageType
import domain.types.MonsterType

class MonsterFactory {
    companion object {

        // https://www.dndwiki.io/monsters/klauth
        fun Klauth() =
            Monster(
                name = "Klauth",
                type = MonsterType.DRAGON,
                immunities = setOf(DamageType.FIRE)
            )

        // https://roll20.net/compendium/dnd5e/Adult%20Bronze%20Dragon#content
        fun Felogos() =
            Monster(
                name = "Felogos",
                immunities = setOf(DamageType.THUNDER),
                type = MonsterType.DRAGON
            )

        // https://www.dndbeyond.com/monsters/16797-balor
        fun Balor() =
            Monster(
                name = "Boze Balor",
                resistances = setOf(DamageType.THUNDER),
                traits = setOf(Trait.MAGIC_RESISTANCE),
                type = MonsterType.FIEND
            )

        // https://www.dndbeyond.com/monsters/16853-earth-elemental
        fun EarthElemental() =
            Monster(
                name = "Earth Elemental",
                vulnerabilities = setOf(DamageType.THUNDER),
                type = MonsterType.ELEMENTAL
            )

        // https://www.dndbeyond.com/monsters/16923-hill-giant
        fun HillGiant() =
            Monster(
                name = "Hendrik de Hill Giant",
                type = MonsterType.GIANT
            )

        // https://roll20.net/compendium/dnd5e/Iron%20Golem#content
        fun IronGolem() =
            Monster(
                name = "Inca de Iron Golem",
                traits = setOf(Trait.MAGIC_RESISTANCE, AbsorptionTrait.FIRE_ABSORPTION),
                type = MonsterType.CONSTRUCT,
                immunities = setOf(DamageType.FIRE, DamageType.POISON, DamageType.PSYCHIC, DamageType.BLUDGEONING, DamageType.PIERCING),
            )

        // http://dnd-5e.herokuapp.com/monsters/death-tyrant
        fun DeathTyrant() =
            Monster(
                name = "Daddy Death Tyrant",
                type = MonsterType.UNDEAD
            )

        // https://www.dndbeyond.com/monsters/16865-flying-sword
        fun FlyingSword() =
            Monster(
                name = "Sting",
                type = MonsterType.CONSTRUCT
            )

        // https://www.dndbeyond.com/monsters/17037-treant
        fun Treant() =
            Monster(
                name = "Boombaard",
                type = MonsterType.PLANT
            )

        // https://www.dndbeyond.com/monsters/17034-tarrasque
        fun Tarrasque() =
            Monster(
                name = "Tarrasque",
                type = MonsterType.MONSTROSITY,
                immunities = setOf(DamageType.FIRE, DamageType.POISON, DamageType.BLUDGEONING, DamageType.PIERCING),
                traits = setOf(Trait.REFLECTIVE_CARAPACE, Trait.MAGIC_RESISTANCE),
            )

        // https://www.aidedd.org/dnd/monstres.php?vo=helmed-horror
        // TODO -> HelmedHorror is immune to multiple, custom chosen, spells.
        fun HelmedHorror() =
            Monster(
                name = "Helmed Horror",
                type = MonsterType.CONSTRUCT,
                immunities = setOf(DamageType.FORCE, DamageType.NECROTIC, DamageType.POISON),
                resistances = setOf(DamageType.BLUDGEONING, DamageType.PIERCING),
                traits = setOf(Trait.MAGIC_RESISTANCE)
            )
    }
}