package controller

import config.BaseTest
import factory.MonsterFactory
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeEach
import rest.OpvragenMonsterRequest
import kotlin.test.assertEquals

class OpvragenMonsterTest : BaseTest() {
    private lateinit var monsterController: MonsterController

    @BeforeEach
    fun setup() {
        monsterController = MonsterController(monsterService)
    }

    @Test
    fun opvragenKlauth() {
        val monster = MonsterFactory.Klauth()
        useMonster(monster)

        val request = OpvragenMonsterRequest(monster.id)
        val response = monsterController.opvragenMonster(request)

        assertEquals(monster.id, response.monster.id)
    }
}