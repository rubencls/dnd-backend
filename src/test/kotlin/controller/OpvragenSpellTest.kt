package controller

import config.BaseTest
import domain.spells.Blight
import domain.spells.MagicMissle
import domain.spells.Thunderwave
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import rest.OpvragenSpellRequest
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class OpvragenSpellTest : BaseTest() {
    private lateinit var spellController: SpellController

    @BeforeEach
    fun setup() {
        spellController = SpellController(spellService)
    }

    @Test
    fun opvragenThunderwave() {
        val spell = Thunderwave()
        useSpell(spell)

        val request = OpvragenSpellRequest(spell.id)

        val result = spellController.opvragenSpell(request)

        assertEquals(8, result.typeOfDiceToThrow)
        assertEquals(2, result.defaultAmountOfDicesToThrow)
        assertTrue(result.shouldDoSavingThrow)
    }

    @Test
    fun opvragenBlight() {
        val spell = Blight()
        useSpell(spell)

        val request = OpvragenSpellRequest(spell.id)

        val result = spellController.opvragenSpell(request)

        assertEquals(8, result.typeOfDiceToThrow)
        assertEquals(8, result.defaultAmountOfDicesToThrow)
        assertTrue(result.shouldDoSavingThrow)
    }

    @Test
    fun opvragenMagicMissle() {
        val spell = MagicMissle()
        useSpell(spell)

        val request = OpvragenSpellRequest(spell.id)

        val result = spellController.opvragenSpell(request)

        assertEquals(8, result.typeOfDiceToThrow)
        assertEquals(3, result.defaultAmountOfDicesToThrow)
        assertFalse(result.shouldDoSavingThrow)
    }

}