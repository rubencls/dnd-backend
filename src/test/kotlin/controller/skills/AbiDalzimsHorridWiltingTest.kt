package controller.skills

import config.BaseTest
import controller.SpellController
import domain.ExtraDamage
import domain.spells.AbiDalzimsHorridWilting
import domain.spells.Spell
import domain.types.DamageType
import factory.MonsterFactory
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import kotlin.test.assertEquals

class AbiDalzimsHorridWiltingTest : BaseTest() {
    private lateinit var spellController: SpellController
    private lateinit var spell: Spell

    @BeforeEach
    fun setup() {
        spellController = SpellController(spellService)

        spell = AbiDalzimsHorridWilting()
        useSpell(spell)
    }

    @Test
    fun onIronGolem() {
        val monster = MonsterFactory.IronGolem()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 1, 0,true)
        val result = spellController.attackMonster(request)

        // Iron Golem is a construct so no damage is done
        assertEquals(0, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onDeathTyrant() {
        val monster = MonsterFactory.DeathTyrant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 64, 0,true)
        val result = spellController.attackMonster(request)

        // Death Tyrant is undead so no damage is done
        assertEquals(0, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onDeathTyrantPlayerBuffedByAbsorbElements() {
        val monster = MonsterFactory.DeathTyrant()
        useMonster(monster)

        val request = buildSimpleRequest(
            monster = monster,
            spell = spell,
            initialDamage = 64,
            damageReduction = 0,
            succeedsSavingThrow = false,
            extraDamage = listOf(ExtraDamage(6, DamageType.LIGHTNING))
        )
        val result = spellController.attackMonster(request)

        // Abi-Dalzim's Horrid Wilting does not affect undead and thus the extra damage will not take place
        assertEquals(0, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onHillGiant() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 64, 0,false)
        val result = spellController.attackMonster(request)

        // Hill giant will lose constitution saving throw, damage stays full
        assertEquals(64, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onHillGiantPlayerBuffedByAbsorbElements() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(
            monster = monster,
            spell = spell,
            initialDamage = 64,
            damageReduction = 0,
            succeedsSavingThrow = true,
            extraDamage = listOf(ExtraDamage(8, DamageType.LIGHTNING))
        )
        val result = spellController.attackMonster(request)

        // Hill giant wins saving throw so 64 / 2 = 32. Extra damage = 8  totals 40
        assertEquals(40, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onHillGiantDebuffedByBestowCurseAndPlayerBuffedByAbsorbElements() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(
            monster = monster,
            spell = spell,
            initialDamage = 64,
            damageReduction = 0,
            succeedsSavingThrow = false,
            extraDamage = listOf(
                ExtraDamage(6, DamageType.LIGHTNING),
                ExtraDamage(4, DamageType.NECROTIC)
            )
        )
        val result = spellController.attackMonster(request)

        assertEquals(74, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onTarrasquePlayerBuffedByAbsorbElements() {
        val monster = MonsterFactory.Tarrasque()
        useMonster(monster)

        val request = buildSimpleRequest(
            monster = monster,
            spell = spell,
            initialDamage = 64,
            damageReduction = 0,
            succeedsSavingThrow = false,
            extraDamage = listOf(ExtraDamage(6, DamageType.FIRE))
        )
        val result = spellController.attackMonster(request)

        assertEquals(64, result.getDamageByMonsterName(monster.name))
    }
}