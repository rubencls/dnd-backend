package controller.skills

import config.BaseTest
import controller.SpellController
import domain.spells.Blight
import domain.spells.Spell
import factory.MonsterFactory
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import kotlin.test.assertEquals

class BlightTest : BaseTest() {
    private lateinit var spellController: SpellController
    private lateinit var spell: Spell

    @BeforeEach
    fun setup() {
        spellController = SpellController(spellService)

        spell = Blight()
        useSpell(spell)
    }

    @Test
    fun onIronGolem() {
        val monster = MonsterFactory.IronGolem()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 1, 0,true)
        val result = spellController.attackMonster(request)

        // Iron Golem is a construct so no damage is done
        assertEquals(0, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onDeathTyrant() {
        val monster = MonsterFactory.DeathTyrant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 64, 0,true)
        val result = spellController.attackMonster(request)

        // Death Tyrant is undead so no damage is done
        assertEquals(0, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onKlauth() {
        val monster = MonsterFactory.Klauth()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 80, 0,true)
        val result = spellController.attackMonster(request)

        // Klauth wins constitution saving throw. Damage will be halved = 80 / 2 = 40
        assertEquals(40, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onHillGiant() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 64, 0,false)
        val result = spellController.attackMonster(request)

        // Hill giant will lose constitution saving throw, damage stays full
        assertEquals(64, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onTreant() {
        val monster = MonsterFactory.Treant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 64, 0,false)
        val result = spellController.attackMonster(request)

        // Treant is a plant creature which means it will roll with disadvantage. Character will win saving throw which means full damage will be done
        assertEquals(64, result.getDamageByMonsterName(monster.name))
    }
}