package controller.skills

import config.BaseTest
import controller.SpellController
import domain.spells.Spell
import domain.spells.Thunderwave
import factory.MonsterFactory
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ThunderwaveTest : BaseTest() {
    private lateinit var spellController: SpellController
    private lateinit var spell: Spell

    @BeforeEach
    fun setup() {
        spellController = SpellController(spellService)

        spell = Thunderwave()
        useSpell(spell)
    }

    @Test
    fun onKlauth() {
        val monster = MonsterFactory.Klauth()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 24, 0,true)
        val result = spellController.attackMonster(request)

        // Rolled damage will be 3d8 (default = 2 + level 2 spell slot) = 24
        // Klauth wins constitution saving throw. Damage will be halved = 24 / 2 = 12
        assertEquals(12, result.getDamageByMonsterName(monster.name))
        assertTrue(result.logs.isNotEmpty())
    }

    @Test
    fun onHillGiant() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 16, 0,false)
        val result = spellController.attackMonster(request)

        assertEquals(16, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onFelogos() {
        val monster = MonsterFactory.Felogos()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 16, 0,true)
        val result = spellController.attackMonster(request)

        // Zero damage because a bronze dragon (Felogos) is immune to thunder damage
        assertEquals(0, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onBalor() {
        val monster = MonsterFactory.Balor()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 16, 0,true)
        val result = spellController.attackMonster(request)

        // https://rpg.stackexchange.com/questions/68320/how-do-saving-throws-for-half-combine-with-damage-resistance: 1/4 of damage is done because:
        // 1. Balor is resistant to lightning damage
        // 2. Balor will succeed in saving throw
        assertEquals(4, result.getDamageByMonsterName(monster.name))
    }


    @Test
    fun onEarthElemental() {
        val monster = MonsterFactory.EarthElemental()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 16, 0,false)
        val result = spellController.attackMonster(request)

        // Rolled damage will be 2d8 (default = 2) = 16
        // Earth elemental is vulnerable to lightning damage (which Thunderwave does) = 16 * 2 = 32
        // Earth elemental will fail saving throw so no damage halved.
        assertEquals(32, result.getDamageByMonsterName(monster.name))
    }
}