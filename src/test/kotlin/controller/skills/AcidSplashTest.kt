package controller.skills

import config.BaseTest
import controller.SpellController
import domain.spells.AcidSplash
import domain.spells.Spell
import factory.MonsterFactory
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import kotlin.test.assertEquals

class AcidSplashTest : BaseTest() {
    private lateinit var spellController: SpellController
    private lateinit var spell: Spell

    @BeforeEach
    fun setup() {
        spellController = SpellController(spellService)

        spell = AcidSplash()
        useSpell(spell)
    }

    @Test
    fun onHillGiantSucceedsSavingThrow() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 6, 0, true)
        val result = spellController.attackMonster(request)

        assertEquals(0, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onHillGiantFailsSavingThrow() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 6, 0, false)
        val result = spellController.attackMonster(request)

        assertEquals(6, result.getDamageByMonsterName(monster.name))
    }

}