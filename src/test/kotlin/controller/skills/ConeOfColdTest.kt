package controller.skills

import config.BaseTest
import controller.SpellController
import domain.spells.ConeOfCold
import domain.spells.Spell
import factory.MonsterFactory
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import kotlin.test.assertEquals

class ConeOfColdTest : BaseTest() {
    private lateinit var spellController: SpellController
    private lateinit var spell: Spell

    @BeforeEach
    fun setup() {
        spellController = SpellController(spellService)

        spell = ConeOfCold()
        useSpell(spell)
    }

    @Test
    fun onHillGiantSucceedsSavingThrow() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 63, 0, true)
        val result = spellController.attackMonster(request)

        assertEquals(31, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onHillGiantFailsSavingThrow() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 63, 0, false)
        val result = spellController.attackMonster(request)

        assertEquals(63, result.getDamageByMonsterName(monster.name))
    }


}