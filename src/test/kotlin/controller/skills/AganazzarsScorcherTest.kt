package controller.skills

import config.BaseTest
import controller.SpellController
import domain.spells.AganazzarsScorcher
import domain.spells.Spell
import domain.types.DamageType
import factory.MonsterFactory
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import rest.CalculateDamageRequest
import rest.RestTarget
import kotlin.test.assertEquals

class AganazzarsScorcherTest : BaseTest() {
    private lateinit var spellController: SpellController
    private lateinit var spell: Spell

    @BeforeEach
    fun setup() {
        spellController = SpellController(spellService)

        spell = AganazzarsScorcher()
        useSpell(spell)
    }

    @Test
    fun onKlauth() {
        val monster = MonsterFactory.Klauth()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 24, 0,true)
        val result = spellController.attackMonster(request)

        // Klauth is immune to fire damage
        assertEquals(0, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onHillGiant() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 24, 0,false)
        val result = spellController.attackMonster(request)

        assertEquals(24, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onHillGiantAffectedByFleshRot() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = CalculateDamageRequest(
            spell.id,
            listOf(
                RestTarget(
                    monster.id,
                    24,
                    0,
                    false,
                    null,
                    DamageType.values().map { it }.toSet(),
                    monster.immunities,
                    monster.resistances
                )
            )
        )

        val result = spellController.attackMonster(request)

        assertEquals(48, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onPetrifiedHillGiant() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = CalculateDamageRequest(
            spell.id,
            listOf(
                RestTarget(
                    monster.id,
                    24,
                    0,
                    false,
                    listOf(),
                    monster.vulnerabilities,
                    monster.immunities + setOf(DamageType.POISON),
                    DamageType.values().map { it }.toSet()
                )
            )
        )

        val result = spellController.attackMonster(request)

        assertEquals(12, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onFlyingSword() {
        val monster = MonsterFactory.FlyingSword()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 24, 0,true)
        val result = spellController.attackMonster(request)

        // Flying sword wins dexterity saving throw. Damage will be halved to 12
        assertEquals(12, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onIronGolem() {
        val monster = MonsterFactory.IronGolem()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 24, 0,true)
        val result = spellController.attackMonster(request)

        // Iron golem has 'Fire Absorption' feature. Which means the damage in fire is added to the Iron golem HP.
        assertEquals(-24, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onTarrasque() {
        val monster = MonsterFactory.Tarrasque()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 24, 0,true)
        val result = spellController.attackMonster(request)

        // Zero damage because Aganazzars Scorcher is a 'line' spell which does no damage against monsters with special ability 'Reflective Carapace'
        assertEquals(0, result.getDamageByMonsterName(monster.name))
    }
}