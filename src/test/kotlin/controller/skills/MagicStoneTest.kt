package controller.skills

import config.BaseTest
import controller.SpellController
import domain.spells.MagicStone
import domain.spells.Spell
import factory.MonsterFactory
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import kotlin.test.assertEquals

class MagicStoneTest : BaseTest() {
    private lateinit var spellController: SpellController
    private lateinit var spell: Spell

    @BeforeEach
    fun setup() {
        spellController = SpellController(spellService)

        spell = MagicStone()
        useSpell(spell)
    }

    @Test
    fun onHillGiant() {
        val monster = MonsterFactory.HillGiant()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 6, 0)
        val result = spellController.attackMonster(request)

        assertEquals(6, result.getDamageByMonsterName(monster.name))
    }

    @Test
    fun onHelmedHorror() {
        val monster = MonsterFactory.HelmedHorror()
        useMonster(monster)

        val request = buildSimpleRequest(monster, spell, 6, 5)
        val result = spellController.attackMonster(request)

        assertEquals(0, result.getDamageByMonsterName(monster.name))
    }

}