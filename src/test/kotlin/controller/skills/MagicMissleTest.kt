package controller.skills

import config.BaseTest
import controller.SpellController
import domain.spells.MagicMissle
import domain.spells.Spell
import factory.MonsterFactory

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import rest.CalculateDamageRequest
import rest.RestTarget
import kotlin.test.assertEquals

class MagicMissleTest : BaseTest() {
    private lateinit var spellController: SpellController
    private lateinit var spell: Spell

    @BeforeEach
    fun setup() {
        spellController = SpellController(spellService)

        spell = MagicMissle()
        useSpell(spell)
    }

    @Test
    fun onMultipleMonsters() {
        val firstMonster = MonsterFactory.HillGiant()
        useMonster(firstMonster)

        val secondMonster = MonsterFactory.HelmedHorror()
        useMonster(secondMonster)

        val thirdMonster = MonsterFactory.Tarrasque()
        useMonster(thirdMonster)

        val request = CalculateDamageRequest(
            spell.id,
            listOf(
                RestTarget(firstMonster.id, 5, 0,false, listOf(), firstMonster.vulnerabilities, firstMonster.immunities, firstMonster.resistances),
                RestTarget(secondMonster.id, 3, 4,true, listOf(), secondMonster.vulnerabilities, secondMonster.immunities, secondMonster.resistances),
                RestTarget(thirdMonster.id, 6, 0,true,  listOf(), thirdMonster.vulnerabilities, thirdMonster.immunities, thirdMonster.resistances)
            )
        )
        val result = spellController.attackMonster(request)

        assertEquals(5, result.getDamageByMonsterName(firstMonster.name))

        // Helmed Horror is immune to force damage thus damage will be 0
        assertEquals(0, result.getDamageByMonsterName(secondMonster.name))

        // Terrasque has 'Reflective Carapace' so damage will be 0
        assertEquals(0, result.getDamageByMonsterName(thirdMonster.name))
    }
}
