package controller

import config.BaseTest
import factory.MonsterFactory
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeEach
import rest.CreateMonsterRequest
import rest.toRest
import kotlin.test.assertEquals

class CreateMonsterTest : BaseTest() {
    private lateinit var monsterController: MonsterController

    @BeforeEach
    fun setup() {
        monsterController = MonsterController(monsterService)
    }

    @Test
    fun createKlauth() {
        val monster = MonsterFactory.Klauth()

        val request = CreateMonsterRequest(monster.toRest())
        val response = monsterController.createMonster(request)

        assertEquals(monster.name, response.name)
    }
}