package config

import domain.ExtraDamage
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever
import repository.SpellRepository
import service.SpellService
import domain.Monster
import domain.spells.Spell
import repository.MonsterRepository
import rest.CalculateDamageRequest
import rest.RestTarget
import service.MonsterService

open class BaseTest {
    // Repositories
    private val spellRepository = mock(SpellRepository::class.java)
    private val monsterRepository = mock(MonsterRepository::class.java)

    // Services
    val spellService = SpellService(spellRepository, monsterRepository)
    val monsterService = MonsterService(monsterRepository)

    fun useSpell(spell: Spell) =
        whenever(spellRepository.getSpellById(spell.id)).thenReturn(spell)

    fun useMonster(monster: Monster) {
        whenever(monsterRepository.getMonsterById(monster.id)).thenReturn(monster)
        whenever(monsterRepository.save(monster)).thenReturn(monster)
    }

    fun buildSimpleRequest(monster: Monster, spell: Spell, initialDamage: Int, damageReduction: Int, succeedsSavingThrow: Boolean? = null, extraDamage: List<ExtraDamage>? = null) =
        CalculateDamageRequest(
            spell.id,
            listOf(
                RestTarget(
                    monster.id,
                    initialDamage,
                    damageReduction,
                    succeedsSavingThrow,
                    extraDamage,
                    monster.vulnerabilities,
                    monster.immunities,
                    monster.resistances
                )
            )
        )
}