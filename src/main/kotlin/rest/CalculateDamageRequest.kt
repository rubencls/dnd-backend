package rest

import java.util.UUID

data class CalculateDamageRequest(
    val spellId: UUID,
    val targets: List<RestTarget>
)