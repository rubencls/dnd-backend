package rest

import domain.ITrait
import domain.types.DamageType
import domain.types.MonsterType
import java.util.UUID

data class RestMonster(
    val id: UUID,
    val name: String,
    val immunities: Set<DamageType> = HashSet(),
    val resistances: Set<DamageType> = HashSet(),
    val vulnerabilities: Set<DamageType> = HashSet(),
    val traits: Set<ITrait> = HashSet(),
    val type: MonsterType
)