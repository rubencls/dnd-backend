package rest


class CalculateDamageResponse(
    val logs: ArrayList<String> = ArrayList()
) {
    val damagesDone: MutableList<DamageDone> = mutableListOf()

    fun addLog(log: String) {
        logs.add(log)
    }

    fun getDamageByMonsterName(name: String): Int {
        return damagesDone.find { it.monsterName === name }!!.damage
    }
}

data class DamageDone(
    val monsterName: String,
    var damage: Int
)