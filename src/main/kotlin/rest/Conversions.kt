package rest

import domain.Monster
import domain.Target

fun RestTarget.toTarget() = Target(
    monsterId = this.monsterId,
    initialDamage = this.initialDamage,
    damageReduction = this.damageReduction,
    succeedsSavingThrow = this.succeedsSavingThrow,
    vulnerabilities = this.vulnerabilities,
    immunities = this.immunities,
    resistances = this.resistances,
    extraDamage = this.extraDamage
)

fun Monster.toRest() = RestMonster(
    id = this.id,
    name = this.name,
    immunities = this.immunities,
    resistances = this.resistances,
    vulnerabilities = this.vulnerabilities,
    traits = this.traits,
    type = this.type
)

fun RestMonster.toMonster() = Monster(
    name = this.name,
    immunities = this.immunities,
    resistances = this.resistances,
    vulnerabilities = this.vulnerabilities,
    traits = this.traits,
    type = this.type
)