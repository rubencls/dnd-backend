package rest

data class OpvragenSpellResponse(
    val defaultAmountOfDicesToThrow: Int,
    val typeOfDiceToThrow: Int,
    val shouldDoSavingThrow: Boolean
)