package rest

import domain.ExtraDamage
import domain.types.DamageType
import java.util.*

data class RestTarget(
    val monsterId: UUID,
    val initialDamage: Int,
    val damageReduction: Int,
    val succeedsSavingThrow: Boolean?,
    val extraDamage: List<ExtraDamage>?,
    val vulnerabilities: Set<DamageType>,
    val immunities: Set<DamageType>,
    val resistances: Set<DamageType>
)

