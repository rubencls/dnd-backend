package rest

import java.util.UUID

data class OpvragenMonsterRequest(
    val id: UUID
)