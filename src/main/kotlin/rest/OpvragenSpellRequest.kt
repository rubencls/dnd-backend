package rest

import java.util.UUID

data class OpvragenSpellRequest(
    val id: UUID
)