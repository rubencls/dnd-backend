package rest

data class CreateMonsterRequest(
    val monster: RestMonster
)
