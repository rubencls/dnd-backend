package rest

import java.util.*

data class CreateMonsterResponse(
    val id: UUID = UUID.randomUUID(),
    val name: String
)
