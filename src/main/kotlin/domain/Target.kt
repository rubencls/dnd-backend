package domain

import domain.types.DamageType
import java.util.*

class Target(
    val monsterId: UUID,
    val initialDamage: Int,
    val damageReduction: Int,
    val succeedsSavingThrow: Boolean?,
    val extraDamage: List<ExtraDamage>?,
    val vulnerabilities: Set<DamageType>,
    val immunities: Set<DamageType>,
    val resistances: Set<DamageType>
) {

    fun isVulnerableTo(damageType: DamageType): Boolean =
        vulnerabilities.contains(damageType)

    fun isImmuneTo(damageType: DamageType): Boolean =
        immunities.contains(damageType)

    fun isResistantTo(damageType: DamageType): Boolean =
        resistances.contains(damageType)
}

data class ExtraDamage(
    val damage: Int,
    val type: DamageType
)