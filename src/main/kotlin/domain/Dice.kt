package domain

class Dice(
    val numberIdentifier: Int
) {
    companion object {
        val D4: Dice = Dice(4)
        val D6 = Dice(6)
        val D8: Dice = Dice(8)
        val D20: Dice = Dice(20)
    }
}