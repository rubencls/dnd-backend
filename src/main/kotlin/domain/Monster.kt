package domain

import domain.types.DamageType
import domain.types.MonsterType
import java.util.UUID

class Monster(
    val id: UUID = UUID.randomUUID(),
    val name: String,
    val immunities: Set<DamageType> = HashSet(),
    val resistances: Set<DamageType> = HashSet(),
    val vulnerabilities: Set<DamageType> = HashSet(),
    val traits: Set<ITrait> = HashSet(),
    val type: MonsterType
) {
    fun isConstruct(): Boolean =
        type === MonsterType.CONSTRUCT

    fun isUndead(): Boolean =
        type === MonsterType.UNDEAD

    fun isPlant(): Boolean =
        type === MonsterType.PLANT

    fun hasReflectiveCarapace(): Boolean =
        traits.contains(Trait.REFLECTIVE_CARAPACE)

    fun absorbsDamageOfType(damageType: DamageType): Boolean {
        return traits.filterIsInstance<AbsorptionTrait>().find { it.damageType == damageType } != null
    }

}