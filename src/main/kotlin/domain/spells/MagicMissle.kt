package domain.spells

import domain.types.DamageType
import domain.Dice
import domain.Monster

class MagicMissle : Spell(
    damageType = DamageType.FORCE,
    amountOfDices = 3,
    dice = Dice.D8,
    hasSavingThrow = false
) {

    override fun hasEffectOn(monster: Monster): Boolean {
        return ! monster.hasReflectiveCarapace()
    }

}