package domain.spells

import domain.types.DamageType
import domain.Dice
import domain.types.SavingThrowEffectType

class ConeOfCold : Spell(
    damageType = DamageType.FIRE,
    amountOfDices = 8,
    dice = Dice.D8,
    hasSavingThrow = true,
    savingThrowEffectType = SavingThrowEffectType.HALF_DAMAGE
)