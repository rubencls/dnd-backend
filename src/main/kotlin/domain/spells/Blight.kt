package domain.spells

import domain.types.DamageType
import domain.Dice
import domain.Monster
import domain.types.SavingThrowEffectType

// https://www.dndbeyond.com/spells/blight
class Blight : Spell(
    damageType = DamageType.NECROTIC,
    amountOfDices = 8,
    dice = Dice.D8,
    hasSavingThrow = true,
    savingThrowEffectType = SavingThrowEffectType.HALF_DAMAGE
) {

    override fun hasEffectOn(monster: Monster): Boolean {
        return ! ( monster.isConstruct() || monster.isUndead() )
    }

}