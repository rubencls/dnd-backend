package domain.spells

import domain.types.DamageType
import domain.Dice

class MagicStone : Spell(
    damageType = DamageType.BLUDGEONING,
    amountOfDices = 1,
    dice = Dice.D6,
    hasSavingThrow = false
)