package domain.spells

import domain.types.DamageType
import domain.Dice
import domain.types.SavingThrowEffectType

class AganazzarsScorcher : Spell(
    damageType = DamageType.FIRE,
    amountOfDices = 3,
    dice = Dice.D8,
    hasSavingThrow = true,
    savingThrowEffectType = SavingThrowEffectType.HALF_DAMAGE
)