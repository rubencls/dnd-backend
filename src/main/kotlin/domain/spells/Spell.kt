package domain.spells

import domain.Dice
import domain.Monster
import domain.types.DamageType
import domain.types.SavingThrowEffectType
import java.util.*

abstract class Spell(
    val id: UUID = UUID.randomUUID(),
    val damageType: DamageType,
    var amountOfDices: Int,
    val dice: Dice,
    val hasSavingThrow: Boolean,
    val savingThrowEffectType: SavingThrowEffectType? = null
) {
    open fun hasEffectOn(monster: Monster): Boolean = true
}