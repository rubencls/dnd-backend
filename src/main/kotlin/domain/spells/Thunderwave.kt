package domain.spells

import domain.types.DamageType
import domain.Dice
import domain.types.SavingThrowEffectType

class Thunderwave : Spell(
    damageType = DamageType.THUNDER,
    amountOfDices = 2,
    dice = Dice.D8,
    hasSavingThrow = true,
    savingThrowEffectType = SavingThrowEffectType.HALF_DAMAGE
)