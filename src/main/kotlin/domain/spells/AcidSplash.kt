package domain.spells

import domain.types.DamageType
import domain.Dice
import domain.types.SavingThrowEffectType

class AcidSplash : Spell(
    damageType = DamageType.FIRE,
    amountOfDices = 1,
    dice = Dice.D6,
    hasSavingThrow = true,
    savingThrowEffectType = SavingThrowEffectType.NEGATE_DAMAGE
)