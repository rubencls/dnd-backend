package domain

import domain.types.DamageType

interface ITrait {
}
enum class Trait : ITrait{
    REFLECTIVE_CARAPACE,
    MAGIC_RESISTANCE,
}

enum class AbsorptionTrait(val damageType: DamageType) : ITrait {
    FIRE_ABSORPTION(DamageType.FIRE)
}