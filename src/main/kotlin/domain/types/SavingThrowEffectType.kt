package domain.types

enum class SavingThrowEffectType{
    NEGATE_DAMAGE,
    HALF_DAMAGE
}