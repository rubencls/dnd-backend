package domain.types

enum class MonsterType {
    CONSTRUCT,
    DRAGON,
    FIEND,
    ELEMENTAL,
    GIANT,
    UNDEAD,
    PLANT,
    MONSTROSITY
}