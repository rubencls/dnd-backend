package domain.types

enum class DamageType(val isPhysical: Boolean) {
    THUNDER(false),
    NECROTIC(false),
    FIRE(false),
    POISON(false),
    PSYCHIC(false),
    BLUDGEONING(true),
    PIERCING(true),
    FORCE(false),
    LIGHTNING(false),
    SLASHING(true)
}