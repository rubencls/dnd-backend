package repository

import domain.Monster
import domain.types.MonsterType
import java.util.*

class MonsterRepository {
    fun getMonsterById(id: UUID): Monster {
        // Mocked for now
        return Monster(id = id, name = "Mock", type = MonsterType.CONSTRUCT)
    }

    fun save(monster: Monster): Monster {
        // Mocked for now
        return monster
    }
}