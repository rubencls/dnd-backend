package controller

import rest.CalculateDamageResponse
import rest.*
import service.SpellService

class SpellController(
    private val spellService: SpellService
) {
    fun attackMonster(request: CalculateDamageRequest): CalculateDamageResponse {
        return spellService.attackMonster(request)
    }

    fun opvragenSpell(request: OpvragenSpellRequest): OpvragenSpellResponse {
        return spellService.opvragenSpell(request)
    }
}