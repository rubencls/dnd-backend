package controller

import rest.*
import service.MonsterService

class MonsterController(
    private val monsterService: MonsterService
) {
    fun createMonster(request: CreateMonsterRequest): CreateMonsterResponse {
        val monster = monsterService.createMonster(request)

        return CreateMonsterResponse(
            monster.id,
            monster.name
        )
    }

    fun opvragenMonster(request: OpvragenMonsterRequest): OpvragenMonsterResponse {
        return OpvragenMonsterResponse(
            monsterService.opvragenMonster(request).toRest()
        )
    }

}