package service

import domain.Monster
import domain.Target
import domain.spells.Spell
import domain.types.DamageType
import domain.types.SavingThrowEffectType
import repository.MonsterRepository
import repository.SpellRepository
import rest.*

class SpellService(
    private val spellRepository: SpellRepository,
    private val monsterRepository: MonsterRepository
) {
    fun attackMonster(request: CalculateDamageRequest): CalculateDamageResponse {
        val spell = spellRepository.getSpellById(request.spellId)
        val response = CalculateDamageResponse()

        for (target in request.targets.map { it.toTarget() }) {
            val monster = monsterRepository.getMonsterById(target.monsterId)
            val damage = calculateDamage(spell, monster, response, target)
            response.damagesDone.add(DamageDone(monster.name, damage))
        }

        return response
    }

    fun opvragenSpell(request: OpvragenSpellRequest): OpvragenSpellResponse {
        val spell = spellRepository.getSpellById(request.id)

        return OpvragenSpellResponse(spell.amountOfDices, spell.dice.numberIdentifier, spell.hasSavingThrow)
    }

    private fun calculateDamage(
        spell: Spell,
        monster: Monster,
        response: CalculateDamageResponse,
        target: Target
    ): Int {
        if (!spell.hasEffectOn(monster)) {
            response.addLog("Spell does not have effect on monster. Zero damage done.")
            return 0
        }

        var dmg = calculateBasedOnDamageType(target, monster, spell.damageType, target.initialDamage, spell.hasSavingThrow, response, spell)

        if (target.extraDamage != null) {
            target.extraDamage.forEach {
                dmg += calculateBasedOnDamageType(target, monster, it.type, it.damage, false, response, spell)
            }
        }

        return dmg

    }

    private fun calculateBasedOnDamageType(target: Target, monster: Monster, damageType: DamageType, initialDamage: Int, hasSavingThrow: Boolean, response: CalculateDamageResponse, spell: Spell): Int {
        var damage = initialDamage

        if (monster.absorbsDamageOfType(damageType)) {
            response.addLog("Spell deals fire damage and monster has fire absorption. Monster will heal by $damage")
            return -damage
        }

        if (target.isImmuneTo(damageType)) {
            response.addLog("Target is immune to $damageType. Zero damage done.")
            return 0
        }

        // First calculate damage from saving throw
        if (hasSavingThrow) {
            if (target.succeedsSavingThrow!!) {
                when (spell.savingThrowEffectType!!) {
                    SavingThrowEffectType.HALF_DAMAGE -> {
                        val futureDamage = (damage * 0.5).toInt()
                        response.addLog("Monster won saving throw! Damage $damage will be halved to $futureDamage")
                        damage = futureDamage
                    }
                    SavingThrowEffectType.NEGATE_DAMAGE -> {
                        response.addLog("Monster won saving throw! Damage will be negated to zero")
                        return 0
                    }
                }
            } else {
                response.addLog("Caster won saving throw! Current calculated damage: $damage")
            }
        }

        // Then calculate damage from damage reduction
        if (damageType.isPhysical) {
            damage -= target.damageReduction
        }

        // Then calculate damage from resistance / vulnarabilty
        if (target.isResistantTo(damageType)) {
            val futureDamage = (damage * 0.5).toInt()
            response.addLog("Target is resistant to $damageType. Damage will be halved from $damage to $futureDamage")
            damage = futureDamage
        }

        if (target.isVulnerableTo(damageType)) {
            val futureDamage = damage * 2
            response.addLog("Target is vulnerable to $damageType. Damage will be doubled from $damage to $futureDamage")
            damage = futureDamage
        }

        return damage
    }
}