package service

import domain.Monster
import repository.MonsterRepository
import rest.*

class MonsterService(
    private val monsterRepository: MonsterRepository
) {

    fun createMonster(request: CreateMonsterRequest): Monster {
        val monster = request.monster.toMonster()
        monsterRepository.save(monster)
        return monster
    }

    fun opvragenMonster(request: OpvragenMonsterRequest): Monster {
        return monsterRepository.getMonsterById(request.id)
    }

}