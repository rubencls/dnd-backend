# Spells to implement
 - Chromatic orb -> Character is able to choose which type of damage the spell does
 - Acid Arrow -> Ranged Spell Attack?

# Debuffs to consider
- Bestow curse, spell does extra necrotic damage

# Features to implement
 - One log per monster or one containing process of every monster?
 - Direction of spell (line, cone) for reflective carapace
 - Reporting on why a spell didn't have an effect on a monster
 - Mechanism to handle monsters which are immune to certain spells (Like Helmed Horror)

# Business rules to implement
 - Monster names should be unique
 - Character names should be unique
 - When a saving throw is used, the type of saving throw must be specified

# Tests to add
- Blight - A plant type creature (rolls with disadvantage) with the magic resistance feature (rolls with advantage), what takes priority?
- Blight - Monster with plant type that is immune necrotic damage. Does blight do maximum damage or no damage?
- Test that fails when switching vulnarability and saving throw calculation

# Things to figure out
- Do I really have to create every monster? That takes alot of work before starting the battle. I need to figure out how to minimize load on the user
- Diseases
- What happens when a monster is vulnarable to a damagetype and then gets petrified?
- What happens with extra damage if the saving throw is succesfull? DOes the extra damage also gets halved?
- Magic damage reduction? (https://www.dandwiki.com/wiki/Damage_Reduction_(5e_Variant_Rule))
- Negative damage? (https://rpg.stackexchange.com/questions/108178/is-there-an-official-rule-that-the-minimum-damage-cant-be-negative)